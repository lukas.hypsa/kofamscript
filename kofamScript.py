#!/usr/bin/python
import glob, os # we import libraries
os.chdir(".") # setting working directory for fcs in 'os' library

kfilePath = "bvitam.txt"
#TODO print("Input file:") read inp  
kFile = open(kfilePath, 'r')
LinesList = kFile.readlines() # save all lines of kFile into array of string
kDict = {} # 'key -> value' dictionary, for saving 'k' as keys and their score as value 

# 1. get all K types from given file with list of K types
for line in LinesList: # traversing lines of kFile
    if(not(line) or (line == "\n") or (line[0] != 'K')): # if blank line or new line or it doesnt start with char 'K!, we continue to next iteration of loop (aka next string from the string array)
        continue
    if(line[-1] == "\n"): # remove end lines
            line = line[0:-2]
    k = line
    kDict[k] = 0

# check the read K types
for k, v in kDict.items():
    print(k,"=",v,";")

print() # just printing a new line
# 2. for each kofam file, extract the best scores into the dictionary & save the dictionary values
extensionString = ".kofam"
for file in glob.glob("*" + extensionString):
    print("working with komal file:", file)
    file1 = open(file, 'r')
    LinesList = file1.readlines()
    count = 0
    for line in LinesList:
        if (count >= 5): # for testing purpose, we work only with first 5 lines
            break
        # here we traverse one file line by line
        numOfLineParts = 6
        strParts = line.split(maxsplit=numOfLineParts) # split the single line into 6 parts
        if(strParts[6][-1] == "\n"): # remove end lines
            strParts[6] = strParts[6][0:-2]
        for part in strParts:
            print(part+";", end='')
        count += 1
        print()

        num3Str = strParts[3] # check, if the 3. part is number (and not a '-')
        if(num3Str == '-'):
            result = -1
            print("\tnum3 not a number")
        else:
            num3 = float(num3Str) # compute the percentage score
            num4 = float(strParts[4])
            print("\tnum3, num4: ",num3,num4)
            onePercent = (num3/100)
            result = (num4/onePercent)
        print("\tresult: %.2f" % result) # here we have the score
        # TODO if the score higher than the one in dictionary, save as new score 
    # TODO save the generated dictionary to file
